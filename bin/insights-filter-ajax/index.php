<?php
	sleep(2);
	//$json = array("black_listed" => false, "unavailable" => false);
	//$json = array("isVerified" => true, "url" => "http://www.google.com");
	$json = array (
		"total" => 20,
		"moreResults" => true,
		"listingType" => "mediacenter",
		"sort" => array (
				"rootName" => "Sort By",
				"sortedBy" => "Date Added",
				"tags" => array (
						"/sort/sort1" => "Sort 1",
						"/sort/sort2" => "Sort 2",
						"/sort/sort3" => "Sort 3" 
				) 
		),
		"filters" => array (
				"0" => array (
						"filtered" => false,
						"rootPath" => "/etc/tags/marsh-industry",
						"rootName" => "Industry",
						"tags" => array (
								"/etc/tags/marsh-industry/tag1" => "Tag 1",
								"/etc/tags/marsh-industry/tag2" => "Tag 2" 
						) 
				),
				"1" => array (
						"filtered" => false,
						"rootPath" => "/etc/tags/region",
						"rootName" => "Region",
						"tags" => array (
								"/etc/tags/marsh-region/tag3" => "Tag 3",
								"/etc/tags/marsh-region/tag4" => "Tag 4" 
						) 
				),
				"2" => array (
						"filtered" => false,
						"rootPath" => "/etc/tags/marsh-riskissue",
						"rootName" => "Risk Issue",
						"tags" => array (
								"/etc/tags/marsh-riskissue/tag5" => "Tag 5",
								"/etc/tags/marsh-riskissue/tag6" => "Tag 6",
								"/etc/tags/marsh-riskissue/tag7" => "Tag 7",
								"/etc/tags/marsh-riskissue/tag8" => "Tag 8" 
						) 
				),
				"3" => array (
						"filtered" => true,
						"rootPath" => "/etc/tags/marsh-test",
						"rootName" => "Test Category",
						"tags" => array(
							"etc/tags/marsh-test/tag9" => "Filtered 9"
						)
				)
				/*"3" => array (
						"filtered" => false,
						"rootPath" => "/etc/tags/marsh-riskissue",
						"rootName" => "Test Category",
						"tags" => array (
								"/etc/tags/marsh-riskissue/tag5" => "Test Tag",
								"/etc/tags/marsh-riskissue/tag6" => "Tag 10",
								"/etc/tags/marsh-riskissue/tag7" => "Tag 11",
								"/etc/tags/marsh-riskissue/tag8" => "Tag 12"
						)
				)*/
				/*"4" => array (
						"filtered" => true,
						"rootPath" => "/etc/tags/marsh-test",
						"rootName" => "Test Category 2",
						"tags" => array(
								"etc/tags/marsh-test/tag10" => "Filtered 10"
						)
				)*/
		),
		"clearAll" => "Clear All",
		//"href" => "resource-centers-html/insights-center/infinite-scroll-content/resource-set-page-1.html" 
		"href" => "resource-centers-html/insights-center/infinite-scroll-content/resource-set-page-1.php" 
		
);
	print json_encode($json);
?>

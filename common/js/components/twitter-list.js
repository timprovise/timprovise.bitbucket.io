;(function( $ ) {
    $.fn.tickertweet = function( options ) {
        var _t = this;
        var currTweet = 0;
        var $tweetList = $('#tweetList > li');
        $tweetList.eq(currTweet).addClass("fade-in");
        function twitterTick() {
            if ($tweetList[currTweet] !== $tweetList[$tweetList.length - 1]) {                
                $tweetList.eq(currTweet).removeClass("fade-in");
                currTweet++;
                $tweetList.eq(currTweet).addClass("fade-in");
            } else {
                $tweetList.eq(currTweet).removeClass("fade-in");
                currTweet = 0;
                $tweetList.eq(currTweet).addClass("fade-in");
            }
        }
        return _t.each(function(){
        	var tweetRefreshInterval = $('#tweet-display-interval').data('tweetdisplayinterval') ? ($('#tweet-display-interval').data('tweetdisplayinterval'))*1000 : 4000;
            setInterval(twitterTick, parseInt(tweetRefreshInterval));
        });
    };
})(jQuery);

$(document).ready(function() {
	if($('#twitter-username').length != 0) {
		var runmodes = $('.runmodes').text() ? $('.runmodes').text() : 'publish';
		var isEditMode = runmodes.indexOf('author') !== -1;

		if (!isEditMode) {
			if (typeof(Storage) !== "undefined") {
				var twitterRefreshRate = $('#twitter-refresh-rate').data('twitterrefreshrate') ? $('#twitter-refresh-rate').data('twitterrefreshrate') : 900;
				var tweetLocalStorage = 'tweets_' + $('#twitter-username').data('twitterusername') + '_' + $('#site-type').data('sitetype');
				var tweetTimeoutSessionStorage = 'tweetsTimeout_' + $('#twitter-username').data('twitterusername') + '_' + $('#site-type').data('sitetype');

				if ((sessionStorage.getItem(tweetTimeoutSessionStorage) != null) && (((new Date().getTime()/1000) - sessionStorage.getItem(tweetTimeoutSessionStorage)) < twitterRefreshRate) && (localStorage.getItem(tweetLocalStorage) != null)) {
					showTweets(localStorage.getItem(tweetLocalStorage));
				} else {
					getNewTweets(true, $('#site-type').data('sitetype'));
				}
			} else {
				getNewTweets(false, $('#site-type').data('sitetype'));
			}
		} else {
			getNewTweets(false, $('#site-type').data('sitetype'));
		}
	}
});

function getNewTweets(setdata, sitetype) {
	var url = $('#twitter-url').data('twitterurl');
	var tweetLocalStorage = 'tweets_' + $('#twitter-username').data('twitterusername') + '_' + sitetype;
	var tweetTimeoutSessionStorage = 'tweetsTimeout_' + $('#twitter-username').data('twitterusername') + '_' + sitetype;

	$.ajax({
		type : 'GET',
		url : url,
		cache: false,
		success : function(data) {
			showTweets(data.trim());
			if (setdata) {
				if (data.trim().length > 0) {
					if (localStorage.getItem(tweetLocalStorage) != null) {
						localStorage.removeItem(tweetLocalStorage);
					}
					localStorage.setItem(tweetLocalStorage, data.trim());

					if (sessionStorage.getItem(tweetTimeoutSessionStorage) != null) {
						sessionStorage.removeItem(tweetTimeoutSessionStorage);
					}
					sessionStorage.setItem(tweetTimeoutSessionStorage, (new Date().getTime())/1000);
				}
			}
		}
	});
}

function showTweets(tweetData) {
    $('#twitter-url').replaceWith(tweetData);
    $("#tweetList").tickertweet();
    $('#site-type').remove();
    $('#twitter-username').remove();
    $('#twitter-refresh-rate').remove();
}
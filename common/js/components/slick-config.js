/*
*
* Slick Slider/Carousel initializer and other configurations
*
*/
$(document).ready(function(e) {
	$('.feature-carousel .carousel-panel').slick(
	{
		infinite: true,
		slidesToShow: 2,
		speed:1000,
		slidesToScroll: 1,
		responsive: [
		{
			breakpoint: 1025,
			settings: {
				arrows: false,
				dots: true
			}
		},
		{
			breakpoint: 693,
			settings: {
				slidesToShow: 1,
				slidesToScroll: 1,
				arrows: false,
				dots: true
			}
		}
		]
	});

	$('.promo-slider .slider').each(function(idx, item){
		var carouselId = "sliderId-" + idx;
		this.id = carouselId;
		$(this).slick({
			slide: "#" + carouselId + " .slide",
			infinite: true,
			slidesToShow: 1,
			slidesToScroll: 1,
			prevArrow: '<span class="slide-prev">' + $("#" + carouselId + " .prevCtrl").val() + '</span>',
			nextArrow: '<span class="slide-next">' + $("#" + carouselId + " .nextCtrl").val() + '</span>',
			appendArrows: "#" + carouselId + " .slider-control"
		});
	});

	$('.full-width-carousel .slider').slick({
	  infinite: true,
	  slidesToShow: 1,
	  slidesToScroll: 1,
	  responsive: [

		{
			breakpoint: 1025,
			settings: {
				arrows: false,
				dots: true
			}
		},
		{
			breakpoint: 693,
			settings: {
				slidesToShow: 1,
				slidesToScroll: 1,
				arrows: false,
				dots: true
			}
		}
		]
	});
    $('.homepage-feature-area .slider').slick({
	  infinite: true,
		autoplay: false,
		// add
		// autoplay: true,
  	// autoplaySpeed: 8000,
		// speed:1000,
		// fade: true,
  	// cssEase: 'linear',
		//
	  slidesToShow: 1,
	  slidesToScroll: 1,
      dots:true,
      arrows: false
	  //responsive: [
            /*{
                breakpoint: 1024,
                settings: {
                    //slidesToShow: 3,
                    //slidesToScroll: 3,
                    //infinite: true,
                    //arrows: true
                }
            },
            {
                breakpoint: 1025,
                settings: {
                    arrows: false,
                    dots: true
                }
            },
            {
                breakpoint: 693,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    arrows: false,
                    dots: true
                }
            }*/
		//]
	});
	$('.text-slider').slick({
	  infinite: true,
	  slidesToShow: 1,
	  slidesToScroll: 1,
	  responsive: [
	   {
		breakpoint: 1025,
			settings: {
				arrows: false,
				dots: true
			}
		}
	  ]

	});

});

/*
 *
 * Pop Modal
 * 
 * 
 */
;(function( $ ) {
	$.fn.popper = function( options ) {
		//set default accessible options
		var defaults = {
				bgName: "reveal-modal-bg-white",
				modalId: "#modalWindow",
				modalClass: ".modal",
				fireType:"",
				modalType: "default",				
				closeModal: ".status-icon",
				dynModalData:"modal-dialog",
				altClass: "",
				dynamicEvent: false,//add a dynamic Event listener for the customClone feature, i.e. jQuery .on method or other function
				customClone: false,
				animSpeed:250,
				openCallback: function(){}
		};
		var settings = $.extend( {}, defaults, options );
		//initializing private variables
		var src,
		content = {}, 
		flag = true,
		bgClass = "." + settings.bgName,
		$modalWindow = $(settings.modalId),
		bgEl = $(document.createElement("div")),
		modalName = settings.modalClass.split(".").join(""),
		thisHref;		
		// helper function for transition startes - extend to apply various animations
		function transitionAnim(param, dir) {			
			if (dir === "out") {
				var $pop = $(param + " " + settings.modalClass);
				// FADE
				$(param).fadeOut(settings.animSpeed, 
						function() {
					$(this).css({display: "none", visibility: "invisible"});
					$pop.remove();
				});				
			} else if (dir === "in") {
				$(param).css({visibility: "visible", left:0}).fadeIn(settings.animSpeed);
			}
		}

		//check whether the background element has been created and add it, if necessary
		if ( !$(bgClass).length ) {
			bgEl.addClass(settings.bgName).appendTo('body');
		} else {
			bgEl = $("." + settings.bgName);
		}
		//close function. 
		//Hide background and remove injected HTML elements
		var _close = function() {			
			bgEl.fadeOut(settings.animSpeed);
			transitionAnim(settings.modalId, "out");
		};
		//open function
		//Add missing event listeners and open the lightbox by appending to the modalWindow id
		var _open = function(self){
			if(flag) {
				var $modalWindow = $(settings.modalId);

				// click on modal fires nothing
				$modalWindow.on("click", settings.modalClass, function(e) {e.stopPropagation();});

				// click on close icon fires close
				$modalWindow.on("click", settings.closeModal, function(e) {_close();});

				// enhance js for opti-in; include option for disabling modal close
				if (settings.modalType === "disableClose") {
					$(".reveal-modal-bg-white").off("click");          
				} else {
					$(".reveal-modal-bg-white, " + settings.modalId).on("click", //".modal",						
							function(e) {
						if ( !$(e.target).is(settings.modalId + " " + settings.modalClass) ) {
							_close();
						}
					});
				}
				if ($("a"+ settings.modalClass ).length ) {
					$( "a" + settings.modalClass ).click(function(e) {
						e.preventDefault();
					});
				}
				flag = false;
			}
			//custom modalTypes can be defined to manipulate the value of the content var
			//if no modalType is defined by user, it will use the default content process
			//adding another if else here with a new modalType string will extend and customize
			//the data associated with that modalType            
			if ( settings.modalType === "iframe" ) {
				src = self.find(".video");
				var srcPath = src.data("src");
				var container = $("<div class='i-container modal'>" + "<span class='status-icon'>" + "</span>" + "</div");
				var $iframeEl = $(document.createElement("iframe"));
				$iframeEl = $iframeEl.attr({
					"src": srcPath,
					"webkitallowfullscreen": "",
					"mozallowfullscreen": "",
					"allowfullscreen": ""
				});
				content = container.append($iframeEl);
			}
			else if (settings.modalType === "lazyimages") {
				src = self.find(".lazyimg");//.data("src");
				var srcPath = src.data("src");
				var hrefPath = src.data("href");
				var $aTag1 = $(document.createElement("a"));                
				$aTag1 = $aTag1.attr({
					"href" : hrefPath,
					"target" : "_blank"                    
				});                
				var $imgEl = $(document.createElement("img"));
				$imgEl = $imgEl.attr({
					"src": srcPath,
					"alt": "modal image"
				});                
				content = self.clone().append($imgEl);
				$imgEl.wrap($aTag1);
			}
			else if ( settings.customClone ) {
				content = $(settings.customClone).clone().attr("class", settings.modalName);
				$(settings.modalId).addClass(settings.altClass);
			}
			else if ( settings.modalType === "urlCheck" ) {
				content = $("#dynDialogModal").clone().addClass(modalName);				
				var confirmBtn = $(content).find(".c-modal-message__body-ui-confirm");
				var cancelBtn = $(content).find(".c-modal-message__body-ui-cancel");				
				confirmBtn.on('click', function(){
					if ($(self).attr("target") === "_blank") {
						window.open(thisHref);
						_close();
					} else {
						window.location.href = thisHref;
					}					
				});
				cancelBtn.on('click', function(){
					_close();
				});				
			}
			// set for auto fire and agreement verification
			// automatic contact us policy for Japan
			else if ( settings.fireType === "auto" ) {
				$(".reveal-modal-bg-white").off("click");
				$(settings.modalId).addClass(settings.altClass);

				content = $(self).clone().addClass(modalName);				
				var confirmBtn = $(content).find(".c-modal-message__body-ui-confirm");
				var cancelBtn = $(content).find(".c-modal-message__body-ui-cancel");
				var referrer = document.referrer;
				confirmBtn.on('click', function(){
					_close();
				});
				cancelBtn.on('click', function(){
					if (!(referrer.indexOf(document.domain) > -1)) {
						window.location = getRedirectionForEntryRamp();
					} else {
						if(referrer == window.location.href) {
							window.location = getRedirectionForEntryRamp();
						}else {
							window.location = referrer;
						}
					}
				});
			}
			else if ( settings.modalType === "default" ) {
				content = self.clone();			
			}
			bgEl.fadeIn(settings.animSpeed, function(){				
				content.appendTo(settings.modalId).addClass("open");
				transitionAnim(settings.modalId, "in");
				settings.openCallback();				
			});			
		};

		// if auto		
		if (this.attr('data-dyn') === "autoModal") {
			var $self = $(this);
			_open($self);
		}
		// fire for modal message, on element
		if (this.attr('data-dyn') === settings.dynModalData) {
			this.on( "click", function(e) {			
				e.preventDefault();
				e.stopPropagation();
				thisHref = $(this).attr('href');
				var $self = $(this);
				_open($self);
			});
			// fire when on parent element
		} else {					
			this.on( "click", settings.modalClass, function(e){
				e.preventDefault();			
				var $self = $(this);
				_open($self);
			});
		}		
		return this;
	};
})(jQuery);

function getRedirectionForEntryRamp() {
	var tempCurrentPath = window.location.pathname;
	if(tempCurrentPath != undefined && tempCurrentPath != '/') {
		var currentPagePath = tempCurrentPath.split('.html')[0];
		if(currentPagePath.charAt(3) == '/') {
			if(currentPagePath.charAt(6) == '/') {
				tempCurrentPath = currentPagePath.substring(0,7);
			}else {
				tempCurrentPath = currentPagePath.substring(0,4);
			}
		}else {
			tempCurrentPath = '/';
		}
	}else {
		tempCurrentPath = '/';
	}
	return tempCurrentPath;
}

function checkHTTPTag() {
	var $aTags = $('body').find('a');			
	$aTags.each(function(){
		var $this = $(this);
		var $parent = $this.parent();
		var $thisHref = $(this).attr('href');

		$thisHref = String($thisHref);
		// pass any elements with js conflicts and undefined hrefs
		if (!($this.hasClass('has-sub') || $parent.hasClass('has-sub') || 
				$this.hasClass('modal') || $(this).attr('href') === undefined || 
				$(this).attr('href') === "" || $(this).attr('href').match("^#") || 
				$(this).attr('href').indexOf("online.marsh.com/") >= 0)) {
			if ((!($thisHref.substring(0,1) === '/') && !($thisHref.indexOf(document.domain) > -1)) || 
					(document.domain.indexOf('www.marsh.com') > -1 && document.location.pathname.substring(0,4) === '/jp/' && !($thisHref.substring(0,4) === '/jp/'))) {
				$(this).attr('data-dyn','modal-dialog');
			}
		} else {
			return;
		}
	});						
	$("[data-dyn='modal-dialog']").popper({
		modalType: "urlCheck"
	});	
};
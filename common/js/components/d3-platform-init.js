var d3Comp = d3Comp || {};
d3Comp.init = function(settings) {
  d3Comp.linkStyle(settings);   
  if (typeof settings.win.d3 === "undefined") {
    console.log("lib undefined");
    d3Comp.loadLib(settings);   
  }
  if (typeof settings.win.d3 !== "undefined") {
    console.log("lib defined");
    d3Comp.loadComp(settings);
  }
}
d3Comp.linkStyle = function(params){    
  var linkElem = document.createElement('link');
  document.getElementsByTagName('head')[0].appendChild(linkElem);
  linkElem.rel = 'stylesheet';
  linkElem.type = 'text/css';
  linkElem.href = (params.path + params.css);
}
d3Comp.loadLib = function(params){
  $.when(
    $.getScript((params.path + params.lib)),
    $.Deferred(function( deferred ){
        $( deferred.resolve );
    })
  ).done(function(){
    console.log("lib defined");
    d3Comp.loadComp(params);
  });
}
d3Comp.loadComp = function(params){
  $.when(
    $.getScript((params.path + params.comp)),
    $.Deferred(function( deferred ){      
        $( deferred.resolve );        
    })
    ).done(function(){
      params.win[params.func]((params.path + params.data), params.elem, params.options);
      d3Comp.chartRefire(params);
    });
}
d3Comp.chartRefire = function(params){
  var resizeTimer;
  $(params.win).on('resize', function(e) {
    clearTimeout(resizeTimer);
    resizeTimer = setTimeout(function() {
      params.win[params.func]((params.path + params.data), params.elem, params.options);
    }, 300);
  });  
}